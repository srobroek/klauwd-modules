
#

define klauwd_users::group (
  $ensure = hiera('present', 'present'),
  $gid     = undef,
) {
  group { $name:
    ensure => $ensure,
    gid    => $gid
  }
}

