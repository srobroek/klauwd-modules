#



define klauwd_users::user (
  $ensure           = hiera('ensure','present'),
  $comment          = hiera('comment'),
  $email            = undef,
  $groups           = hiera_hash('groups', ['users']),
  $pubkeys          = hiera_hash('pubkeys',[]),
  $uid              = undef,
  $gid              = undef,
  # $home             = hiera('managehome', "/home/${name}"),
  $managehome       = hiera('managehome', true),
  # $gen_key          = hiera('gen_key', false),
  # $add_keys_to_root = hiera('add_keys_to_root', false),
  # $home_mode        = hiera('home_mode','0700'),
  $genpass = generate ('/bin/sh', '-c', "makepasswd --maxchars=10 --minchars=7 | tr -d '\n'"),
  



)

{


  user { $name:
    ensure     => $ensure,
    comment    => $comment,
    groups     => $groups,
    managehome => $managehome,
  } ~>

  exec { "output_initial_password_for_${name}":
    command     => "/bin/echo the initial password for ${name} is ${genpass}",
    onlyif      => "/bin/grep -Eq '^${name}:[*!]!?:' '/etc/shadow'",
    require     => User [ $name ],
    refreshonly => true,
    logoutput   => true,
  } ~>


  exec { "set_initial_password_for_${name}":
    command     => "/bin/echo ${name}:${genpass} |chpasswd",
    onlyif      => "/bin/grep -Eq '^${name}:[*!]!?:' '/etc/shadow'",
    refreshonly => true,
    require     => User [ $name ],
  }

}

