# == Class klauwd_users::params
#
# This class is meant to be called from users.
# It sets variables according to platform.
#
class klauwd_users::params {
  case $::osfamily {
    'Debian': {
      $package_name = 'users'
      $service_name = 'users'
    }
    'RedHat', 'Amazon': {
      $package_name = 'users'
      $service_name = 'users'
    }
    default: {
      fail("${::operatingsystem} not supported")
    }
  }
}
