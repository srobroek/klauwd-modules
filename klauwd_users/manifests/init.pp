# == Class: klauwd_users
#
# Full description of class users here.
#
# === Parameters
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#
class klauwd_users (

) inherits ::klauwd_users::params {

  # validate parameters here

  class { '::klauwd_users::install': } ->
  class { '::klauwd_users::config': } ~>
  class { '::klauwd_users::service': } ->
  Class['::klauwd_users']
  create_resources('klauwd_users::user', hiera_hash('users'), hiera_hash('user_defaults'))
  create_resources('klauwd_users::group', hiera_hash('groups'), hiera_hash('group_defaults'))


}