require 'spec_helper'

describe 'users' do
  context 'supported operating systems' do
    ['Debian', 'RedHat'].each do |osfamily|
      describe "users class without any parameters on #{osfamily}" do
        let(:params) {{ }}
        let(:facts) {{
          :osfamily => osfamily,
        }}

        it { should compile.with_all_deps }

        it { should contain_class('users::params') }
        it { should contain_class('users::install').that_comes_before('users::config') }
        it { should contain_class('users::config') }
        it { should contain_class('users::service').that_subscribes_to('users::config') }

        it { should contain_service('users') }
        it { should contain_package('users').with_ensure('present') }
      end
    end
  end

  context 'unsupported operating system' do
    describe 'users class without any parameters on Solaris/Nexenta' do
      let(:facts) {{
        :osfamily        => 'Solaris',
        :operatingsystem => 'Nexenta',
      }}

      it { expect { should contain_package('users') }.to raise_error(Puppet::Error, /Nexenta not supported/) }
    end
  end
end
